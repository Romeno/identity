"""Identity URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from django.views.generic import TemplateView
from IdentMain.views import index, rights, psy, edu, joinus, news, article, event, report, contact, subscribe, search, articles, archive, error_handler, form_test
from solid_i18n.urls import solid_i18n_patterns
import django.views.defaults

urlpatterns = solid_i18n_patterns(
    '',
    url(r'^$', index, name='index'),
    url(r'^about$', TemplateView.as_view(template_name="about.html"), name='about'),
    url(r'^rights$', rights, name='rights'),
    url(r'^psy$', psy, name='psy'),
    url(r'^edu$', edu, name='edu'),
    url(r'^joinus$', TemplateView.as_view(template_name="volunteer.html"), name='volunteer'),

    url(r'^article/(?P<id>\d+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[a-zA-Z0-9\-_]+)$', article, name='article'),
    url(r'^event/(?P<id>\d+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[a-zA-Z0-9\-_]+)$', event, name='event'),

    url(r'^search$', search, name='search'),
    url(r'^subscribe', subscribe, name='subscribe'),

    url(r'^news$', news, name='news'),
    url(r'^articles$', articles, name='articles'),
    url(r'^archive$', archive, name='archive'),

    url(r'^article$', article, name='article'),
    url(r'^form_test$', form_test, name='form_test'),

    # url(r'^$', TemplateView.as_view(template_name="index.html"), name="index"),
    # url(r'^about/', TemplateView.as_view(template_name="about.html"), name="about"),
    # url(r'^rights/', rights, name="rights"),
    # url(r'^psy/', psy, name="psy"),
    # url(r'^edu/', edu, name="edu"),
    # url(r'^joinus/', joinus, name="joinus"),
    # url(r'^news/', news, name="news"),
    # url(r'^article/', article, name="article"),
    #
    # url(r'^articles/', ArticleList.as_view(), name="articles"),
    #
    # # form submissions
    # url(r'^report/', report, name="report"),
    # url(r'^contact/', contact, name="contact"), # 2 form types
    # url(r'^subscribe/', subscribe, name="subscribe"),
    # url(r'^search/', search, name="search"),


    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

from filebrowser.sites import site

urlpatterns += solid_i18n_patterns(
    '',
    url(r'^admin/filebrowser/', include(site.urls)),
)


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += solid_i18n_patterns(
        '',
        url(r'^rosetta/', include('rosetta.urls')),
    )

handler404 = error_handler(404)
handler403 = error_handler(403)
handler500 = error_handler(500)