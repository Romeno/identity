(function() {
    $(document).ready( function() {
        /* Function to dynamically change text-align */
//        $('.article-badge-darken .article-title').hover( function hoverIn() {
//            var e = $(this);
//            setTimeout(function() {
//                e.css('text-align', 'center');
//            }, 150);
//        }, function hoverOut() {
//            var e = $(this);
//            setTimeout(function() {
//                e.css('text-align', 'left');
//            }, 150);
//        })

        /* Initialization of jScrollPane on 'shown' event of bootstrap-select */
        $('.identity-select').on('shown.bs.select', function (e) {
            /* Initializing ALL selects raised issue with advanced search
               It is sliding up-down block and jScrollPane was not showing.
               Added no-scroll attr to just NOT initiaze it */
            if ($(this).attr('no-scroll') === undefined) {

                /* There was a bug. When doing init on every 'shown' of bootstrap-select
                   the width of jScrollPane content area was increasing in IE due to ???
                   So init it only once it is pressed first time */
                if ($(this).attr('scroll-init') === undefined) {

                    /* setting contentWidth property in jScrollPane settings didn't help the aforementioned bug */
//                    var contentWidth = Number($(this).attr('scroll-pane-content-width'));
//                    var settings = {
//                        contentWidth: (isNaN(contentWidth) ? undefined : contentWidth)
//                    };
//                    $(this).parent('.identity-select.open').find('.dropdown-menu.inner').jScrollPane(settings);

                    $(this).parent('.identity-select.open').find('.dropdown-menu.inner').jScrollPane();
                    $(this).attr('scroll-init', true);
                }
            }
        });
    })


}());