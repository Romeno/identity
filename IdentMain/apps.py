__author__ = 'Romeno'
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class IdentityAppConfig(AppConfig):
    name = 'IdentMain'
    verbose_name = _("Identity")


class FlatblocksAppConfig(AppConfig):
    name = 'flatblocks'
    verbose_name = _("Configuration")