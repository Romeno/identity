# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0003_article'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300)),
                ('book', filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name=b'Book', blank=True)),
                ('category', models.ForeignKey(to='IdentMain.Category')),
                ('tags', models.ManyToManyField(to='IdentMain.Tag')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
