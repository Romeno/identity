# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import IdentMain
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0014_auto_20160229_1813'),
    ]


operations = [
    migrations.RemoveField(
        model_name='event',
        name='category',
    ),
    migrations.AlterField(
        model_name='article',
        name='pub_date',
        field=models.DateTimeField(default=IdentMain.utils.today, blank=True),
        preserve_default=True,
    ),
    migrations.AlterField(
        model_name='event',
        name='pub_date',
        field=models.DateTimeField(default=IdentMain.utils.today, blank=True),
        preserve_default=True,
    ),
    ]
