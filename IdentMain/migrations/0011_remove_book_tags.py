# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0010_auto_20160229_1807'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='tags',
        ),
    ]
