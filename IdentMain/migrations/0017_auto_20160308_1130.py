# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filebrowser.fields
import tagulous.models.fields
import IdentMain.utils
import django.utils.timezone
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0016_auto_20160302_1223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='author',
            field=models.CharField(max_length=200, verbose_name='Author'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='category',
            field=models.ForeignKey(verbose_name='Category', to='IdentMain.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='change_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Last changed'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='content',
            field=tinymce.models.HTMLField(default=b'', verbose_name='Content'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='creation_date',
            field=models.DateTimeField(verbose_name='Creation date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='has_photo',
            field=models.BooleanField(default=False, verbose_name='Has photo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='has_video',
            field=models.BooleanField(default=False, verbose_name='Has video'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='lead',
            field=models.CharField(max_length=500, verbose_name='Annotation'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='lead_image',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='Badge image', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='pub_date',
            field=models.DateTimeField(default=IdentMain.utils.today, verbose_name='Visible on', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(max_length=200, verbose_name='Readable url part'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='tags',
            field=tagulous.models.fields.TagField(help_text='Enter a comma-separated tag string', to='IdentMain._Tagulous_Article_tags', verbose_name='Tags', _set_tag_meta=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='book',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='File', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='category',
            field=models.ForeignKey(verbose_name='Category', to='IdentMain.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='tags',
            field=tagulous.models.fields.TagField(help_text='Enter a comma-separated tag string', to='IdentMain._Tagulous_Book_tags', verbose_name='Tags', _set_tag_meta=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='title',
            field=models.CharField(max_length=300, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(default=None, max_length=70, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='change_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Last changed'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='content',
            field=tinymce.models.HTMLField(default=b'', verbose_name='Content'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='When happen'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='lead',
            field=models.CharField(max_length=500, verbose_name='Annotation'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='pub_date',
            field=models.DateTimeField(default=IdentMain.utils.today, verbose_name='Visible on', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(max_length=200, verbose_name='Readable url part'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='tags',
            field=tagulous.models.fields.TagField(help_text='Enter a comma-separated tag string', to='IdentMain._Tagulous_Event_tags', verbose_name='Tags', _set_tag_meta=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usefullink',
            name='title',
            field=models.CharField(max_length=100, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usefullink',
            name='url',
            field=models.URLField(max_length=300, verbose_name='Url'),
            preserve_default=True,
        ),
    ]
