# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0002_event'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('content', tinymce.models.HTMLField(default=b'')),
                ('lead', models.CharField(max_length=500)),
                ('pub_date', models.DateTimeField()),
                ('last_change_data', models.DateTimeField()),
                ('slug', models.SlugField(max_length=200)),
                ('author', models.CharField(max_length=200)),
                ('creation_date', models.DateTimeField()),
                ('has_photo', models.BooleanField(default=False)),
                ('has_video', models.BooleanField(default=False)),
                ('lead_image', filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name=b'Image', blank=True)),
                ('category', models.ForeignKey(to='IdentMain.Category')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
