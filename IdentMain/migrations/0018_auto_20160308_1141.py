# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import filebrowser.fields
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0017_auto_20160308_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='badge_design',
            field=models.CharField(default=b're', max_length=2, verbose_name='Badge design', choices=[(b're', 'Regular'), (b'da', 'Darken')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 8, 8, 41, 49, 234000, tzinfo=utc), verbose_name='Creation date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='has_photo',
            field=models.BooleanField(default=False, verbose_name='Has photo'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='has_video',
            field=models.BooleanField(default=False, verbose_name='Has video'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='lead_image',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='Badge image', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='category',
            field=models.ForeignKey(verbose_name='Category', to='IdentMain.Category'),
            preserve_default=True,
        ),
    ]
