# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0008_article_badge_design'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='tags',
            field=models.ManyToManyField(to='IdentMain.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=models.ManyToManyField(to='IdentMain.Tag'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='badge_design',
            field=models.CharField(default=b're', max_length=2, verbose_name='Badge design', choices=[(b're', 'Regular'), (b'da', 'Darken')]),
            preserve_default=True,
        ),
    ]
