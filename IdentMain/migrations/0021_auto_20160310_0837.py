# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import IdentMain.utils
import filebrowser.fields
import django.utils.timezone
import tagulous.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0020_auto_20160309_2259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creation date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='lead_image',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='Badge image'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='pub_date',
            field=models.DateTimeField(default=IdentMain.utils.today, verbose_name='Visible on'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='tags',
            field=tagulous.models.fields.TagField(to='IdentMain._Tagulous_Article_tags', blank=True, help_text='Enter a comma-separated tag string', _set_tag_meta=True, null=True, verbose_name='Tags'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='book',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='File'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='tags',
            field=tagulous.models.fields.TagField(to='IdentMain._Tagulous_Book_tags', blank=True, help_text='Enter a comma-separated tag string', _set_tag_meta=True, null=True, verbose_name='Tags'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creation date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='lead_image',
            field=filebrowser.fields.FileBrowseField(max_length=500, null=True, verbose_name='Badge image'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='pub_date',
            field=models.DateTimeField(default=IdentMain.utils.today, verbose_name='Visible on'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='tags',
            field=tagulous.models.fields.TagField(to='IdentMain._Tagulous_Event_tags', blank=True, help_text='Enter a comma-separated tag string', _set_tag_meta=True, null=True, verbose_name='Tags'),
            preserve_default=True,
        ),
    ]
