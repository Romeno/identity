# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0005_auto_20160226_1553'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='last_change_data',
            new_name='change_date',
        ),
        migrations.RenameField(
            model_name='event',
            old_name='last_change_data',
            new_name='change_date',
        ),
    ]
