# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tagulous.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0019_auto_20160309_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='tags',
            field=tagulous.models.fields.TagField(help_text='Enter a comma-separated tag string', to='IdentMain._Tagulous_Book_tags', _set_tag_meta=True, verbose_name='Tags', blank=True),
            preserve_default=True,
        ),
    ]
