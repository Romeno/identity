# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0007_auto_20160229_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='badge_design',
            field=models.CharField(default=b're', max_length=2, choices=[(b're', 'Regular'), (b'da', 'Darken')]),
            preserve_default=True,
        ),
    ]
