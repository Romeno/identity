# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0015_auto_20160302_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='change_date',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='change_date',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
