# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('content', tinymce.models.HTMLField(default=b'')),
                ('lead', models.CharField(max_length=500)),
                ('pub_date', models.DateTimeField()),
                ('last_change_data', models.DateTimeField()),
                ('slug', models.SlugField(max_length=200)),
                ('event_date', models.DateTimeField()),
                ('category', models.ForeignKey(to='IdentMain.Category')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
