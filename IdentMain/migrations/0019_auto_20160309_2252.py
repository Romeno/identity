# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('IdentMain', '0018_auto_20160308_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='change_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 9, 19, 52, 13, 688000, tzinfo=utc), verbose_name='Last changed', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 9, 19, 52, 24, 671000, tzinfo=utc), verbose_name='Creation date'),
            preserve_default=False,
        ),
    ]
