(function() {
    $(document).ready( function() {
        /* Function to dynamically change text-align */
//        $('.article-badge-darken .article-title').hover( function hoverIn() {
//            var e = $(this);
//            setTimeout(function() {
//                e.css('text-align', 'center');
//            }, 150);
//        }, function hoverOut() {
//            var e = $(this);
//            setTimeout(function() {
//                e.css('text-align', 'left');
//            }, 150);
//        })

        /* Initialization of jScrollPane on 'shown' event of bootstrap-select */
        $('.identity-select').on('shown.bs.select', function (e) {
            /* Initializing ALL selects raised issue with advanced search
               It is sliding up-down block and jScrollPane was not showing.
               Added no-scroll attr to just NOT initiaze it */
            if ($(this).attr('no-scroll') === undefined) {

                /* There was a bug. When doing init on every 'shown' of bootstrap-select
                   the width of jScrollPane content area was increasing in IE due to ???
                   So init it only once it is pressed first time */
                if ($(this).attr('scroll-init') === undefined) {

                    /* setting contentWidth property in jScrollPane settings didn't help the aforementioned bug */
//                    var contentWidth = Number($(this).attr('scroll-pane-content-width'));
//                    var settings = {
//                        contentWidth: (isNaN(contentWidth) ? undefined : contentWidth)
//                    };
//                    $(this).parent('.identity-select.open').find('.dropdown-menu.inner').jScrollPane(settings);

                    $(this).parent('.identity-select.open').find('.dropdown-menu.inner').jScrollPane();
                    $(this).attr('scroll-init', true);
                }
            }
        });

        /* Timeline */

        var speed = 0.25;

        $('.timeline-btn').mousedown(function(e) {
            e.preventDefault();

            console.log('down');

            var delta = 0;

            var id = $(this).attr('id');
            var evts = $('.timeline .event');

            if (evts.size() != 0) {
                if (id == 'timeline-prev') {
                    delta = parseFloat($('.timeline .start-marker').css('left')) -  parseFloat($(evts[0]).css('left'))
                } else {
                    delta = parseFloat($('.timeline .end-marker').css('left')) - parseFloat($(evts[evts.size()- 1]).css('left'))
                }
                var t = Math.abs(delta) / speed;

                evts.animate({ "left": "+=" + delta + "px" }, t );
            }
        }).mouseup(function(e) {
            console.log('up');
            e.preventDefault();
        }).click(function(e) {
            console.log('click');
            e.preventDefault();
        });

        $(document).mouseup(function(event) {
            $('.timeline .event').stop();
            console.log('doc up');
        });



    })


}());