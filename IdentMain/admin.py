from django.contrib import admin
from django import forms
import models
from filebrowser.settings import ADMIN_THUMBNAIL
from django.utils.translation import ugettext_lazy as _
import logging


logger = logging.getLogger('identity')


class BaseArticleAdmin(admin.ModelAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(BaseArticleAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'category':
            field.initial = models.Category.objects.get(id=1)
        return field



class ArticleAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('/static/css/fonts/fonts.css', ),
        }
    #     js = ('/static/tiny_mce/tiny_mce.js',
    #           '/static/js/tinymce_setup.js')

    fields = ('title', 'category', 'author', 'lead', 'content', 'pub_date', 'badge_design', 'lead_image', 'has_photo', 'has_video', 'slug', 'creation_date', 'change_date', 'tags',)
    list_display = ('title', 'category', 'author', 'pub_date',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(ArticleAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'category':
            field.initial = models.Category.objects.get(id=1)
        return field

    prepopulated_fields = {"slug": ("title",)}
    readonly_fields = ('creation_date', 'change_date', )


class EventAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('/static/css/fonts/fonts.css', ),
        }
    #     js = ('/static/tiny_mce/tiny_mce.js',
    #           '/static/tinymce_setup/tinymce_setup.js')

    fields = ('title', 'category', 'lead', 'content', 'event_date', 'pub_date', 'badge_design', 'lead_image', 'has_photo', 'has_video', 'slug', 'creation_date', 'change_date', 'tags',)
    list_display = ('title', 'category', 'event_date',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(EventAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'category':
            field.initial = models.Category.objects.get(id=1)
        return field

    prepopulated_fields = {"slug": ("title",)}
    readonly_fields = ('creation_date', 'change_date', )


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'category',)
    readonly_fields = ('creation_date', 'change_date', )

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(BookAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'category':
            field.initial = models.Category.objects.get(id=1)
        return field


from django.contrib.auth.admin import UserAdmin, User


class UsefulLinkAdmin(admin.ModelAdmin):
    list_display = ('title', 'url')

# http://stackoverflow.com/questions/15012235/using-django-auth-useradmin-for-a-custom-user-model
# http://stackoverflow.com/questions/2297377/how-do-i-prevent-permission-escalation-in-django-admin-when-granting-user-chang
class IdentityUserAdmin(UserAdmin):
    model = User
    staff_self_fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        # No permissions
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    staff_other_fieldsets = (
        (None, {'fields': ('username', )}), # no password
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        # No permissions
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    staff_self_readonly_fields = ('last_login', 'date_joined')

    def change_view(self, request, object_id, *args, **kwargs):
        # for non-superuser
        if not request.user.is_superuser:
            try:
                if int(object_id) != request.user.id:
                    self.readonly_fields = User._meta.get_all_field_names()
                    self.fieldsets = self.staff_other_fieldsets
                else:
                    self.readonly_fields = self.staff_self_readonly_fields
                    self.fieldsets = self.staff_self_fieldsets

                response = super(IdentityUserAdmin, self).change_view(request, object_id, *args, **kwargs)
            except:
                logger.error('Admin change view error. Returned all readonly fields')

                self.fieldsets = self.staff_other_fieldsets
                self.readonly_fields = ('first_name', 'last_name', 'email', 'username', 'password', 'last_login', 'date_joined')
                response = super(IdentityUserAdmin, self).change_view(request, object_id, *args, **kwargs)
            finally:
                # Reset fieldsets to its original value
                self.fieldsets = UserAdmin.fieldsets
                self.readonly_fields = UserAdmin.readonly_fields
            return response
        else:
            return super(IdentityUserAdmin, self).change_view(request, object_id, *args, **kwargs)

admin.site.unregister(User)
admin.site.register(User, IdentityUserAdmin)


admin.site.register(models.Article, ArticleAdmin)
admin.site.register(models.Book, BookAdmin)
admin.site.register(models.Event, EventAdmin)
admin.site.register(models.Category)
admin.site.register(models.UsefulLink, UsefulLinkAdmin)
admin.site.register(models.Subscriber)
