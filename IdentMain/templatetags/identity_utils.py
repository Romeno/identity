__author__ = 'Romeno'

from django.template.defaulttags import register
from django import template
from django.conf import settings


register = template.Library()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


ALLOWABLE_VALUES = ("LANGUAGE_SESSION_KEY", )


class SetVarNode(template.Node):

    def __init__(self, var_name, var_value):
        self.var_name = var_name
        self.var_value = var_value

    def render(self, context):
        try:
            value = template.Variable(self.var_value).resolve(context)
        except template.VariableDoesNotExist:
            value = ""
        context[self.var_name] = value
        return u""


@register.tag(name='set')
def set_var(parser, token):
    """
        {% set <var_name>  = <var_value> %}
    """
    parts = token.split_contents()
    if len(parts) < 4:
        raise template.TemplateSyntaxError("'set' tag must be of the form:  {% set <var_name>  = <var_value> %}")
    return SetVarNode(parts[1], parts[3])


@register.assignment_tag
def settings_value(name):
    is_allowable = [x for x in ALLOWABLE_VALUES if x == name]
    if len(is_allowable) > 0:
        return getattr(settings, name, '')
    return ''


@register.filter
def concat(arg1, arg2):
    """concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)