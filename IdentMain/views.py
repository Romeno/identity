from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import ListView
from django.http import HttpResponse, HttpResponsePermanentRedirect, HttpResponseRedirect, StreamingHttpResponse, \
    HttpResponseNotFound
from django.http.response import HttpResponseRedirectBase
from django.template.response import TemplateResponse
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.core import exceptions

from django.core.urlresolvers import reverse

from django.http import Http404, HttpResponseNotAllowed

from models import Article, Event, Book, DummyEvent, UsefulLink
from utils import log_multiple, pick
from django.utils.timezone import now as get_now
import logging
import copy


logger = logging.getLogger('identity')


class HttpResponseRedirect303(HttpResponseRedirectBase):
    status_code = 303


class HttpResponseRedirect308(HttpResponseRedirectBase):
    status_code = 308


#
# class ArticleList(ListView):
# model = Article


def it():
    import time

    d = {}

    yield "<html><body><h3>Bang bang</h3>"

    time.sleep(5)

    yield "<p>Bang bang</p></body></html>"

    time.sleep(5)

    yield d['dasdasd']


def index(req):
    arts = Article.objects.all().order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    def closest():
        if not events:
            return 0

        now = get_now()

        i_res = -1
        for i, e in enumerate(events):
            if e.event_date < now:
                i_res = i
            else:
                break

        return i_res

    def set_pos(n):
        start_pos = 37
        step = 23
        for i, e in enumerate(events):
            e.left = start_pos - step*(n-i)

    def add_dummy(n):
        res = list(events)

        if res:
            if n <= 0:
                res = [DummyEvent() for i in xrange(1 - n)]
                res.extend(list(events))
                n += 1 - n

            if len(res) < n + 1 + 2:
                res.extend([DummyEvent() for i in xrange(n + 1 + 2 - len(res))])

        return res, n

    events = Event.objects.filter(pub_date__lt=get_now()).order_by('event_date')
    i_closest = closest()
    events, i_closest = add_dummy(i_closest)
    set_pos(i_closest)

    return TemplateResponse(req, 'index.html', context={'columns': cols, 'events': events})


def rights(req):
    uls = UsefulLink.objects.all()
    arts = Article.objects.filter(category__id=2).order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'rights.html', {'useful_links': uls, 'columns': cols})


def psy(req):
    arts = Article.objects.filter(category__id=1).order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'psy.html', {'columns': cols})

TRAINING_TAG_NAME = 'training'
HISTORY_TAG_NAME = 'history'
def edu(req):
    training_arts = Article.objects.filter(tags=TRAINING_TAG_NAME).order_by('-pub_date')
    training_cols = pick(training_arts, settings.COLUMN_COUNT)

    history_arts = Article.objects.filter(tags=HISTORY_TAG_NAME).order_by('-pub_date')
    history_columns = pick(history_arts, settings.COLUMN_COUNT)

    books = Book.objects.all().order_by('-creation_date')
    book_columns = pick(books, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'edu.html', {'training_columns': training_cols,
                                              'history_columns': history_columns,
                                              'book_columns': book_columns})


def joinus(req):
    resp = TemplateResponse(req, 'rights.html')


def news(req):
    arts = Article.objects.all().order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'news.html', {'columns': cols})


def article(req, id, year, month, day, slug):
    q = {'pk': int(id),
         # 'creation_date__year': int(year),
         # 'creation_date__month': int(month),
         # 'creation_date__day': int(day),
         # 'slug__iexact': str(slug)
         }

    try:
        a = Article.objects.get(**q)
        return TemplateResponse(req, 'article.html', context={'article': a, 'entity_date': a.pub_date})
    except Article.MultipleObjectsReturned:
        log_multiple(Article, q, id=id, year=year, month=month, day=day, slug=slug)
        raise Http404()
    except Article.DoesNotExist:
        raise Http404()


def event(req, id, year, month, day, slug):
    q = {'pk': int(id),
         # 'creation_date__year': int(year),
         # 'creation_date__month': int(month),
         # 'creation_date__day': int(day),
         # 'slug__iexact': str(slug)
         }

    try:
        a = Event.objects.get(**q)
        return TemplateResponse(req, 'article.html', context={'article': a, 'entity_date': a.event_date})
    except Event.MultipleObjectsReturned:
        log_multiple(Event, q, id=id, year=year, month=month, day=day, slug=slug)
        raise Http404()
    except Event.DoesNotExist:
        raise Http404()


def report(req):
    pass


def contact(req):
    pass


def subscribe(req):
    from models import SubscriberForm

    if req.method == 'POST':
        form = SubscriberForm(req.POST, req.FILES)
        from django.core.files.storage import default_storage

        if form.is_valid():
            form.save()
            return TemplateResponse(req, 'subscribed.html')
        else:
            return HttpResponseRedirect(reverse('index'))
    else:
        return HttpResponseNotAllowed(['POST',])


def search(req):
    return HttpResponseRedirect(reverse('articles'))


def articles(req):
    arts = Article.objects.all().order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'articles.html', {'columns': cols})


def archive(req):
    arts = Article.objects.all().order_by('-pub_date')
    cols = pick(arts, settings.COLUMN_COUNT)

    return TemplateResponse(req, 'archive.html', {'columns': cols})


def form_test(req):
    from models import MyForm
    from django.conf import settings
    import os
    import locale

    print locale.getdefaultlocale()
    print locale.getlocale()

    if req.method == 'POST':
        form = MyForm(req.POST, req.FILES)
        from django.core.files.storage import default_storage

        if form.is_valid():
            uploaded_file = form.cleaned_data['image']
        else:
           return HttpResponse('<html><body>Idi nahui bliad\' dolbaiob!!!!!!!!!!!!!!!! </body></html>')

        fname = default_storage.get_available_name(uploaded_file.name)

        with open(os.path.join(settings.MEDIA_ROOT, fname), 'wb+') as f:
            for chunk in uploaded_file.chunks():
                f.write(chunk)

        return HttpResponse('<html><body>Idi nahui bliad\' dolbaiob!!! </body></html>')
    elif req.method == 'GET':
        form = MyForm()
        resp = TemplateResponse(req, 'tests/formtest.html', {'form': form})

        return resp
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def error_handler(number):
    from django.views.defaults import page_not_found, server_error, permission_denied

    error_views = {
        404: page_not_found,
        403: permission_denied,
        500: server_error
    }

    def error(req):
        from django.utils.translation import activate

        default_view = error_views.get(number)
        if not default_view:
            raise ImproperlyConfigured('No default handler for error number %d defined\
             in IdentMain.views.error_handler' % number)

        lang = req.session[settings.LANGUAGE_SESSION_KEY]
        activate(lang)

        return default_view(req, '%d.html' % number)

    return error


def server_info(req):
    import mod_wsgi
    import django

    c = u''
    c += u'<html><body>\n'

    c += u'mod_wsgi.version (imported from module): %s\n<br>\n' % unicode(mod_wsgi.version)
    c += u'<br>\n'

    c += u'Django version is %s\n<br>\n' % unicode(django.VERSION)
    c += u'<br>\n'

    c += u'Wsgi vars:\n'
    for k in sorted(req.environ.keys()):
        c += u'<br>%s=%s\n' % (k, req.environ[k])
    c += u'<br>\n'

    c += u'</body></html>\n'

    return HttpResponse(c)

