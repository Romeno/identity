__author__ = 'Romeno'

import logging
import datetime
from django.utils.timezone import now, localtime

logger = logging.getLogger('identity')


def get_multiple(model, q):
    return model.filter(**q)


def log_multiple(model, q, **kwargs):
    err_str = 'Query'
    for k, v in kwargs.iteritems():
        err_str += ' %(key)s=%%(%(key)s)s' % {'key': k}
    err_str += ' returned'

    try:
        what_objects = get_multiple(model, q)

        err_str += ' %(num)d objects' % {'num': what_objects.count()}
    except:
        err_str += ' multiple objects. Getting additional info failed.'
    logger.error(err_str)


def pick(iterable, parts):
    """
    Picks elements from iterable to form numParts new lists

    Input:
    [1,2,3,4,5,6,7,] / 4

    Output:
    [[1,5,],
     [2,6,],
     [3,7,],
     [4]]
    """
    inp = list(iterable)
    res = []

    for i in range(parts):
        res.append(inp[i::parts])

    return res


def today():
    return localtime(now()).replace(hour=0, minute=0, second=0, microsecond=0)