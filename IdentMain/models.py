from django.db import models
from tinymce.models import HTMLField
from django import forms
from filebrowser.fields import FileBrowseField
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from utils import today
import tagulous


class Category(models.Model):
    name = models.CharField(max_length=70, default=None, verbose_name=_('Name'))

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class BaseArticle(models.Model):
    REGULAR = 're'
    DARKEN = 'da'
    BADGE_DESGIN = (
        (REGULAR, _('Regular')),
        (DARKEN, _('Darken')),
    )

    slug = models.SlugField(max_length=200, verbose_name=_('Readable url part'))
    category = models.ForeignKey(Category, verbose_name=_('Category'))

    title = models.CharField(max_length=200, verbose_name=_('Title'))
    content = HTMLField(default='', verbose_name=_('Content'))

    lead = models.CharField(max_length=500, verbose_name=_('Annotation'))

    creation_date = models.DateTimeField(default=now, verbose_name=_('Creation date'))
    change_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Last changed'))
    pub_date = models.DateTimeField(default=today, verbose_name=_('Visible on'))

    badge_design = models.CharField(max_length=2, choices=BADGE_DESGIN, default=REGULAR, verbose_name=_('Badge design'))
    has_photo = models.BooleanField(default=False, verbose_name=_('Has photo'))
    has_video = models.BooleanField(default=False, verbose_name=_('Has video'))
    lead_image = FileBrowseField(_("Badge image"), max_length=500, directory="images/",
                                 extensions=settings.FILEBROWSER_EXTENSIONS['Image'],
                                 null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title


class Article(BaseArticle):
    author = models.CharField(max_length=200, verbose_name=_('Author'))

    # cannot move to ArticleBase
    tags = tagulous.models.TagField(blank=True, null=True, verbose_name=_('Tags'))

    def save(self, **kwargs):
        if self.pk is None:
            self.creation_date = now()

        self.change_date = now()
        
        super(Article, self).save(**kwargs)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')


class Event(BaseArticle):
    event_date = models.DateTimeField(default=now, verbose_name=_('When happen'))

    # cannot move to ArticleBase
    tags = tagulous.models.TagField(blank=True, null=True, verbose_name=_('Tags'))
    dummy = False

    def save(self, **kwargs):
        if self.pk is None:
            self.creation_date = now()

        self.change_date = now()

        super(Event, self).save(**kwargs)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')


class DummyEvent(object):
    def __init__(self):
        self.title = ''
        self.pub_date = ''
        self.left = 0
        self.dummy = True

    def __str__(self):
        return "dummy"

    def __unicode__(self):
        return u"dummy"


class Book(models.Model):
    title = models.CharField(max_length=300, verbose_name=_('Title'))
    category = models.ForeignKey(Category, verbose_name=_('Category'))
    book = FileBrowseField(_("File"), max_length=500, directory="books/",
                           extensions=settings.FILEBROWSER_EXTENSIONS['Document'],
                           null=True)
    creation_date = models.DateTimeField(verbose_name=_('Creation date'))
    change_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Last changed'))

    tags = tagulous.models.TagField(blank=True, null=True, verbose_name=_('Tags'))

    def save(self, **kwargs):
        if self.pk is None:
            self.creation_date = now()

        self.change_date = now()

        super(Book, self).save(**kwargs)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Book')
        verbose_name_plural = _('Books')


class UsefulLink(models.Model):
    title = models.CharField(max_length=100, verbose_name=_('Title'))
    url = models.URLField(max_length=300, verbose_name=_('Url'))

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Useful link')
        verbose_name_plural = _('Useful links')


class Subscriber(models.Model):
    fullname = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)

    def __str__(self):
        return self.fullname

    def __unicode__(self):
        return self.fullname

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')

from django.forms import ModelForm


class SubscriberForm(ModelForm):
    class Meta:
        model = Subscriber
        fields = ['fullname', 'email', ]
