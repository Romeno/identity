__author__ = 'Romeno'

from solid_i18n.middleware import SolidLocaleMiddleware
from django.conf import settings


class IdentityLocaleMiddleware(SolidLocaleMiddleware):

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.session[settings.LANGUAGE_SESSION_KEY] = request.LANGUAGE_CODE


from django.http import HttpResponseNotAllowed
from django.template import RequestContext, loader, TemplateDoesNotExist


class HttpResponseNotAllowedMiddleware(object):
    def process_response(self, request, response):
        if isinstance(response, HttpResponseNotAllowed):
            context = RequestContext(request)
            try:
                content = loader.render_to_string("405.html", context_instance=context)
                response.content = content
            except TemplateDoesNotExist, e:
                print "Template does not exist %s" % str(e)
                content = '<html><body>405 - Method not allowed</body></html>'
                response.content = content

        return response