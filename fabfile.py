__author__ = 'Romeno'

from fabric.api import env, run, cd
import server_keys

env.hosts = [server_keys.HOST]
env.user = server_keys.USER_NAME
env.password = server_keys.PASSWORD


def deploy():
    with cd(server_keys.APP_DIR):
        run("git pull")

        run("npm install")
        run("bower install")

        run("pip2.7 install -r ./requirements.txt")
        run("python2.7 ./manage.py collectstatic")
        run("python2.7 ./manage.py migrate")
        # run("python2.7 ./manage.py loaddata -v 3 initial_data")

        # restart server
        run(server_keys.SERVER_MANAGEMENT_DIR + server_keys.RESTART_CMD)

