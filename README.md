﻿This is the package of Identity website

it contains and depends on:
1) Node modules
2) Bower components
3) Pip modules

The installation and preparations is using install.bat

Compiling django messages is using makemessages.bat

Making messages is by compilemessages.bat

./uploaded - Media folder
./templates - Templates folder
./static_all - Folder where static files gets copied
./psds - Folder with all design psds
./locale - Folder with django message files - locale, language files (*.po, *.mo)
./layout - Static fully functional html layout
./IdentMain - Main Django python app
./Identity - Django project
./fonts - all machinery of fonts here
 ├ Katherine - Katherine font
 ├ Museo* - Museo fonts
 └ _Release - web ready fonts (.eot, .otf, .svg, .ttf, .woff, etc) + preview.html 
				and css
   ├ Katherine - Katherine font
   └ Museo Sans Cyrylic - Museo fonts